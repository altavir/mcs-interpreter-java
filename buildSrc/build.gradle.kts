plugins { `kotlin-dsl` }

gradlePlugin {
    plugins {
        register("mcs-build-plugin") {
            id = "mcs-build"
            displayName = "MCS Build Plugin"
            implementationClass = "io.github.commandertvis.mcs.McsBuild"
        }
    }
}

repositories(RepositoryHandler::jcenter)
