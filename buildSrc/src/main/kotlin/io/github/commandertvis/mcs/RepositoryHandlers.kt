package io.github.commandertvis.mcs

import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.kotlin.dsl.maven

public fun RepositoryHandler.mcsInterpreterJavaGitlab(action: MavenArtifactRepository.() -> Unit): MavenArtifactRepository =
    maven(url = "https://gitlab.com/api/v4/projects/18466669/packages/maven", action = action)

public fun RepositoryHandler.jitpack(): MavenArtifactRepository = maven(url = "https://jitpack.io")
