rootProject.name = "mcs-interpreter-java"

pluginManagement {
    plugins {
        val dokkaVersion: String by settings
        val kotlinVersion: String by settings
        id("org.jetbrains.dokka") version dokkaVersion
        kotlin(module = "jvm") version kotlinVersion
    }
}
