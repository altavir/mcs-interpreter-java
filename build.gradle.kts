import io.github.commandertvis.mcs.invoke
import io.github.commandertvis.mcs.jitpack
import io.github.commandertvis.mcs.mcsInterpreterJavaGitlab
import org.jetbrains.dokka.gradle.DokkaTask
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val antlr4Version: String by project
val asmVersion: String by project
val jafamaVersion: String by project
val junitJupiterVersion: String by project
val kotlinApiVersion: String by project
val kotlinJvmTarget: String by project
val kotlinLanguageVersion: String by project
val kotlinVersion: String by project
val mcsInterpreterJavaVersion: String by project
plugins { `maven-publish`; `mcs-build`; antlr; id("org.jetbrains.dokka"); kotlin(module = "jvm") }
description = "MCS interpreter for JVM bytecode."
group = "io.github.commandertvis.mcs"
version = mcsInterpreterJavaVersion
repositories { jcenter(); jitpack(); mavenCentral() }

dependencies {
    antlr("org.antlr:antlr4:$antlr4Version")
    api("net.jafama:jafama:$jafamaVersion")
    api("org.ow2.asm:asm:$asmVersion")
    api("org.ow2.asm:asm-commons:$asmVersion")
    api("org.ow2.asm:asm-util:$asmVersion")
    implementation(kotlin(module = "stdlib-jdk8", version = kotlinVersion))
    testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
    testImplementation(kotlin(module = "test", version = kotlinVersion))
    testImplementation(kotlin(module = "test-junit5", version = kotlinVersion))
}

val antlrOutputDir = "build/generated-src/antlr/main"
kotlin { sourceSets { main { kotlin.srcDir(antlrOutputDir) } } }

tasks {
    dokka.get().outputDirectory = "public/"

    val dokkaJavadoc = task<DokkaTask>(name = "dokkaJavadoc") {
        group = JavaBasePlugin.DOCUMENTATION_GROUP
        outputFormat = "javadoc"
        outputDirectory = javadoc.get().destinationDir.toString()
    }

    val dokkaJar = task<Jar>(name = "dokkaJar") {
        dependsOn(dokkaJavadoc)
        group = JavaBasePlugin.DOCUMENTATION_GROUP
        archiveClassifier("javadoc")
        from(javadoc.get().destinationDir)
    }

    withType<DokkaTask> {
        configuration {
            includes = listOf("PACKAGES.md")
            jdkVersion = 8
            moduleName = "main"
            platform = "JVM"
            targets = listOf("JVM")
            perPackageOption { prefix = "kotlin" }

            sourceLink {
                path = "src/main/kotlin"
                url = "https://gitlab.com/CMDR_Tvis/${project.name}/tree/master/src/main/kotlin/"
                lineSuffix = "#L"
            }
        }
    }

    publish.get().dependsOn(build)
    test.get().useJUnitPlatform()

    generateGrammarSource {
        arguments.addAll(setOf("-long-messages", "-no-listener", "-visitor"))
        outputDirectory = file(antlrOutputDir)
    }

    withType<KotlinCompile> {
        kotlinOptions {
            apiVersion = kotlinApiVersion
            jvmTarget = kotlinJvmTarget
            languageVersion = kotlinLanguageVersion
            suppressWarnings = true
            freeCompilerArgs = listOf("-Xopt-in=kotlin.RequiresOptIn")
        }

        dependsOn(generateGrammarSource)
    }

    publishing {
        publications {
            create<MavenPublication>(name = "mavenJava") {
                from(project.components["java"])
                artifacts { artifact(kotlinSourcesJar.get()); artifact(dokkaJar) }

                pom {
                    packaging = "jar"
                    name(project.name)
                    description(project.description)
                    url("https://gitlab.com/CMDR_Tvis/${project.name}")
                    inceptionYear("2020")

                    licenses {
                        license {
                            comments("Open-source license")
                            distribution("repo")
                            name("MIT License")
                            url("https://gitlab.com/CMDR_Tvis/${project.name}/blob/master/LICENSE.txt")
                        }
                    }

                    developers {
                        developer {
                            email("postovalovya@gmail.com")
                            id("CMDR_Tvis")
                            name("Commander Tvis")
                            roles("architect", "developer")
                            timezone("7")
                            url("https://commandertvis.github.io")
                        }
                    }

                    scm {
                        connection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        developerConnection("scm:git:git@gitlab.com:CMDR_Tvis/${project.name}.git")
                        url("git@gitlab.com:CMDR_Tvis/${project.name}.git")
                    }
                }
            }

            repositories {
                mcsInterpreterJavaGitlab {
                    credentials(credentialsType = HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }

                    authentication { register(name = "header", type = HttpHeaderAuthentication::class) }
                }
            }
        }
    }
}
