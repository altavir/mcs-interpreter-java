package io.github.commandertvis.mcs

import kotlin.contracts.ExperimentalContracts
import kotlin.contracts.InvocationKind
import kotlin.contracts.contract

/**
 * Applies [action] to errors in [McsResult], if they exist.
 *
 * @return this object.
 */
@OptIn(ExperimentalContracts::class)
public inline fun <T> McsResult<T>.ifErrors(action: (List<String>) -> Unit): McsResult<T> {
    contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
    return apply { errors?.let(action) }
}

/**
 * Applies [action] to value of [McsResult], if exists.
 *
 * @return this object.
 */
@OptIn(ExperimentalContracts::class)
public inline fun <T> McsResult<T>.ifValue(action: (T) -> Unit): McsResult<T> {
    contract { callsInPlace(action, InvocationKind.AT_MOST_ONCE) }
    return apply { value?.let(action) }
}
