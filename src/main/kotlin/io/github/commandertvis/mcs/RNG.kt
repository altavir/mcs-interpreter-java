package io.github.commandertvis.mcs

import net.jafama.FastMath

internal class RNG(jump: Int = 0) {
    private var u0: Long = 1L
    private var u1: Long = 0L

    init {
        repeat(jump) { nextDouble() }
    }

    private fun nextDouble(): Double {
        val c0 = m0 * u0
        val c1 = m0 * u1 + m1 * u0
        u0 = c0 - c0.shr(26).shl(26)
        val n = c1 + c0.shr(26)
        u1 = n - n.shr(14).shl(14)
        return u0 * x0 + u1 * x1
    }

    internal fun nextDouble(min: Double, max: Double): Double {
        return min + nextDouble() * FastMath.abs(max - min)
    }

    companion object {
        private const val m0 = 45887173L
        private const val m1 = 11368L
        private const val x0 = 9.094947017729284E-13
        private const val x1 = 6.103515625000004E-5
    }
}
