package io.github.commandertvis.mcs

import McsLexer
import McsParser
import net.jafama.FastMath
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import java.util.concurrent.ConcurrentHashMap
import kotlin.concurrent.thread

/**
 * The MCS interpreter class. It abstracts compilation and running of MCS based expressions and utilization of their
 * values for plotting and building histograms.
 *
 * @param parallelism quantity of threads forked for calculating [histogram] function values.
 * @param cacheSize quantity of cached implementations of expression calculators.
 */
public class McsInterpreter(private val parallelism: Int = 1, private val cacheSize: Int = 64) {
    private val astCache = hashMapOf<String, McsParser.ProgramContext>()
    internal val compilerCache = hashMapOf<String, Calculator>()
    private val generator = McsClassGenerator()
    private val texGenerator = McsTexGenerator()
    private lateinit var hits: MutableMap<Int, Long>
    private val workers = mutableListOf<Thread>()

    init {
        require(parallelism > 0) { "parallelism must be positive" }
        require(cacheSize >= 0) { "cacheSize must be positive" }
    }

    /**
     * Evaluates heights of histogram for a modeling formula declared with expression language.
     *
     * @param expression the expression.
     * @param nValues to quantity of values to group.
     * @param xMin the left border of expression's domain.
     * @param xMin the right border of expression's domain.
     * @param histogramMin the left border of histogram's range.
     * @param histogramMax the right border of histogram's range.
     * @param nBins the quantity of bins in the histogram.
     * @return the list of bins' heights.
     */
    @Suppress("RemoveExplicitTypeArguments")
    @OptIn(ExperimentalStdlibApi::class)
    @Synchronized
    public fun histogram(
        expression: String, nValues: Int, xMin: Double, xMax: Double, histogramMin: Double,
        histogramMax: Double, nBins: Int
    ): McsResult<List<Double>> {
        if (nBins <= 0)
            return McsResult.ofErrors("The quantity of bins must be more than 0.")

        if (nValues <= 0)
            return McsResult.ofErrors("The quantity of values must be more than 0.")

        if (xMin > xMax)
            return McsResult.ofErrors("xMin must be less than xMax.")

        if (histogramMin > histogramMax)
            return McsResult.ofErrors("histogramMin must be less than histogramMax.")

        val result = compileOrGet(expression).ifErrors { return McsResult.ofErrors(it) }
        hits = ConcurrentHashMap<Int, Long>(nBins)
        workers.clear()
        val portionSize = nValues / parallelism
        val histogramAbsoluteRange = FastMath.abs(histogramMax - histogramMin)

        (1 until parallelism).forEach {
            workers += thread(name = "$this-Worker-$it") {
                modelValues(
                    nValues,
                    it,
                    portionSize,
                    result,
                    xMin,
                    xMax,
                    histogramMin,
                    histogramMax,
                    histogramAbsoluteRange,
                    nBins
                )
            }
        }

        modelValues(
            nValues,
            0,
            portionSize + nValues % parallelism,
            result,
            xMin,
            xMax,
            histogramMin,
            histogramMax,
            histogramAbsoluteRange,
            nBins
        )

        workers.forEach(Thread::join)
        val bins = mutableListOf<Double>()
        repeat(nBins) { bins.add(hits.getOrDefault(it, 0L).toDouble() / nValues) }
        return McsResult.ofValue(bins)
    }

    private fun modelValues(
        nValues: Int,
        jumpQty: Int,
        portion: Int,
        result: McsResult<Calculator>,
        xMin: Double,
        xMax: Double,
        histogramMin: Double,
        histogramMax: Double,
        histogramAbsoluteRange: Double,
        nBins: Int
    ) {
        val rng = RNG(jump = nValues * jumpQty)
        val bucket = HashMap<Int, Long>(nBins)

        repeat(portion) portion@{
            val value = result.value!!.evaluate(rng.nextDouble(xMin, xMax))

            if (value !in histogramMin..histogramMax)
                return@portion

            val bin = (-((histogramMin - value) / histogramAbsoluteRange) * nBins).toInt()

            if (bin >= 0)
                bucket[bin] = bucket.getOrDefault(bin, 0L) + 1
        }

        bucket.forEach { (t, u) -> hits[t] = hits.getOrDefault(t, 0L) + u }
    }

    /**
     * Evaluates a set of values of function declared with expression language in a certain interval.
     *
     * @param expression the expression.
     * @param fromInclusive the left border of interval.
     * @param toExclusive the right border of interval.
     * @param nPoints the quantity of points to evaluate between [fromInclusive] and [toExclusive].
     * @return the map with arguments as keys and values as values.
     */
    @Synchronized
    public fun plot(
        expression: String,
        fromInclusive: Double,
        toExclusive: Double,
        nPoints: Int
    ): McsResult<Map<Double, Double>> {
        if (nPoints <= 0)
            return McsResult.ofErrors("The quantity of points must be more than 0.")

        if (fromInclusive >= toExclusive)
            return McsResult.ofErrors("fromInclusive must be less than toExclusive.")

        val result = compileOrGet(expression).ifErrors { return McsResult.ofErrors(it) }
        val map = mutableMapOf<Double, Double>()
        val increment = (toExclusive - fromInclusive) / nPoints.toDouble()

        repeat(nPoints) { i ->
            val x = fromInclusive + i * increment
            map[x] = result.value!!.evaluate(x)
        }

        return McsResult.ofValue(map)
    }

    /**
     * Evaluates a single value of function declared with expression language.
     *
     * @param expression the expression.
     * @param x the argument.
     * @return a [Result] object holding the value.
     */
    @Synchronized
    public fun evaluate(expression: String, x: Double): McsResult<Double> {
        val result = compileOrGet(expression)
        result.ifErrors { return McsResult.ofErrors(it) }.ifValue { return McsResult.ofValue(it.evaluate(x)) }
        error("Illegal McsResult object: $result")
    }

    /**
     * Produces a TeX formula from given MCS expresion.
     *
     * @param expression the expression to convert.
     * @return the TeX formula [String].
     */
    @Synchronized
    public fun produceTex(expression: String): McsResult<String> {
        val program = parse(expression)
            .ifErrors { return McsResult.ofErrors(it) }
            .value
            ?: return McsResult.ofErrors("Illegal parser output")

        return texGenerator.generateTex(program)
    }

    @Synchronized
    private fun parse(expression: String): McsResult<McsParser.ProgramContext> {
        astCache[expression]?.let { return McsResult.ofValue(it) }
        val listener = RecordingErrorListener()

        val parser = McsParser(CommonTokenStream(McsLexer(CharStreams.fromString(expression)).apply {
            removeErrorListeners()
            addErrorListener(listener)
        })).apply {
            removeErrorListeners()
            addErrorListener(listener)
        }

        val program = parser.program() ?: return McsResult.ofErrors("Illegal parser output")

        if (listener.tail.isNotEmpty())
            return McsResult.ofErrors(listener.tail)

        if (astCache.size >= cacheSize)
            astCache -= astCache.keys.first()

        astCache[expression] = program
        return McsResult.ofValue(program)
    }

    @Synchronized
    private fun compileOrGet(expression: String): McsResult<Calculator> {
        compilerCache[expression]?.let { return McsResult.ofValue(it) }

        val program = parse(expression)
            .ifErrors { return McsResult.ofErrors(it) }
            .value
            ?: return McsResult.ofErrors("Illegal parser output")

        val calculator = generator
            .instantiateCalculator(expression, program)
            .ifErrors { return McsResult.ofErrors(it) }

        if (compilerCache.size >= cacheSize)
            compilerCache -= compilerCache.keys.first()

        compilerCache[expression] = calculator.value ?: return McsResult.ofErrors("Illegal compiler output")
        return calculator
    }
}
