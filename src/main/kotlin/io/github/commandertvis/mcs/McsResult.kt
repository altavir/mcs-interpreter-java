package io.github.commandertvis.mcs

/**
 * Wraps a compiler or runtime output: either a useful [value] or list of [errors].
 *
 * @param T the type of useful value.
 * @property errors the errors list.
 * @property value the useful value.
 */
public data class McsResult<T> internal constructor(public val errors: List<String>?, public val value: T?) {
    /**
     * Checks if [errors] exist.
     *
     * @return `true`, if [errors] exist, `false` otherwise.
     */
    public fun hasErrors(): Boolean = errors != null

    /**
     * Checks if [value] exists.
     *
     * @return `true`, if [value] exists, `false` otherwise.
     */
    public fun hasValue(): Boolean = value != null

    public companion object {
        /**
         * Constructs a [McsResult] with [errors] and without [value].
         *
         * @param T the type of useful value.
         * @param errors the errors list.
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun <T> ofErrors(errors: List<String>): McsResult<T> =
            McsResult(errors = errors, value = null)

        /**
         * Constructs a [McsResult] with [value] and without [errors].
         *
         * @param T the type of useful value.
         * @param value the value.
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun <T> ofValue(value: T): McsResult<T> = McsResult(errors = null, value = value)

        /**
         * Constructs a [McsResult] with [errors] and without [value].
         *
         * @param T the type of useful value.
         * @param errors the errors list.
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun <T> ofErrors(vararg errors: String): McsResult<T> = ofErrors(errors.toList())

        /**
         * Constructs a [McsResult] with [errors] and without [value].
         *
         * @param T the type of useful value.
         * @param error the error.
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun <T> ofErrors(error: String): McsResult<T> = ofErrors(listOf(error))

        /**
         * Constructs a [McsResult] with [errors] and without [value].
         *
         * @param T the type of useful value.
         * @param errors the errors list.
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun <T> ofErrors(errors: Iterable<String>): McsResult<T> = ofErrors(errors.toList())

        /**
         * Constructs a [Unit] [McsResult] without any errors.
         *
         * @return a new [McsResult].
         */
        @JvmStatic
        public fun ofValue(): McsResult<Unit> = ofValue(Unit)
    }
}
