package io.github.commandertvis.mcs

import McsParser
import McsParserBaseVisitor

public class McsTexGenerator : McsParserBaseVisitor<McsResult<Unit>>() {
    private lateinit var tail: StringBuilder
    private var parentForFunctionIsExponentiation = false
    private var childFunctionTookExponentiation = false

    @Synchronized
    internal fun generateTex(ctx: McsParser.ProgramContext): McsResult<String> {
        tail = StringBuilder()
        visitProgram(ctx)
        return McsResult.ofValue(tail.toString())
    }

    public override fun visitProgram(ctx: McsParser.ProgramContext): McsResult<Unit> =
        visitExpression(ctx.expression()!!)

    private fun visitExpression(ctx: McsParser.ExpressionContext): McsResult<Unit> = when (ctx) {
        is McsParser.ExponentiationContext -> visitExponentiation(ctx)
        is McsParser.MultiplicationContext -> visitMultiplication(ctx)
        is McsParser.DivisionContext -> visitDivision(ctx)
        is McsParser.AdditionContext -> visitAddition(ctx)
        is McsParser.SubtractionContext -> visitSubtraction(ctx)
        is McsParser.NumberContext -> visitNumber(ctx)
        is McsParser.EContext -> visitE(ctx)
        is McsParser.PiContext -> visitPi(ctx)
        is McsParser.ArgumentContext -> visitArgument(ctx)
        is McsParser.SubexpressionContext -> visitSubexpression(ctx)
        is McsParser.FunctionCallContext -> visitFunctionCall(ctx)
        else -> McsResult.ofErrors("Illegal expression context: $ctx")
    }

    public override fun visitFunctionCall(ctx: McsParser.FunctionCallContext): McsResult<Unit> {
        val name = ctx.IDENTIFIER()!!.text
        val arity = ctx.expression().size

        (adapterOf(name, arity)
            ?: return McsResult.ofErrors("Cannot find function $name with arity $arity"))(ctx.expression())

        return McsResult.ofValue()
    }

    public override fun visitSubexpression(ctx: McsParser.SubexpressionContext): McsResult<Unit> {
        val errors = mutableListOf<String>()
        visitSign(ctx.sign()!!).ifErrors { errors.addAll(it) }
        tail.append('(')
        visitExpression(ctx.expression()!!).ifErrors { errors.addAll(it) }

        if (errors.isNotEmpty())
            return McsResult.ofErrors(errors)

        tail.append(')')
        return McsResult.ofValue()
    }

    public override fun visitArgument(ctx: McsParser.ArgumentContext): McsResult<Unit> {
        visitSign(ctx.sign()!!).ifErrors { return McsResult.ofErrors(it) }
        tail.append("x")
        return McsResult.ofValue()
    }

    public override fun visitExponentiation(ctx: McsParser.ExponentiationContext): McsResult<Unit> {
        val left = ctx.expression(0)!!
        val right = ctx.expression(1)!!

        if (left is McsParser.FunctionCallContext) {
            val target = StringBuilder(tail)
            tail = StringBuilder()
            visitExpression(right).ifErrors { return McsResult.ofErrors(it) }
            val rightCache = StringBuilder(tail)
            parentForFunctionIsExponentiation = true
            visitExpression(left).ifErrors { return McsResult.ofErrors(it) }

            if (childFunctionTookExponentiation) {
                target.append(tail)
                tail = target
                return McsResult.ofValue()
            }

            target.append(tail)
            target.append("^{")
            target.append(rightCache)
            target.append('}')
            tail = target
            childFunctionTookExponentiation = false
            return McsResult.ofValue()
        }

        val errors = mutableListOf<String>()
        visitExpression(left).ifErrors { errors.addAll(it) }
        tail.append("^{")
        visitExpression(right).ifErrors { errors.addAll(it) }

        if (errors.isNotEmpty())
            return McsResult.ofErrors(errors)

        tail.append("}")
        return McsResult.ofValue()
    }

    private fun evaluateSign(ctx: McsParser.SignContext): Boolean {
        val sign = ctx.text

        if (sign.count { it == '-' } % 2 == 0)
            return true

        return false
    }

    public override fun visitSign(ctx: McsParser.SignContext): McsResult<Unit> {
        if (!evaluateSign(ctx))
            tail.append('-')

        return McsResult.ofValue()
    }

    public override fun visitPi(ctx: McsParser.PiContext): McsResult<Unit> {
        visitSign(ctx.sign()!!).ifErrors { return McsResult.ofErrors(it) }
        tail.append("\\pi")
        return McsResult.ofValue()
    }

    public override fun visitE(ctx: McsParser.EContext): McsResult<Unit> {
        visitSign(ctx.sign()!!).ifErrors { return McsResult.ofErrors(it) }
        tail.append("e")
        return McsResult.ofValue()
    }

    public override fun visitNumber(ctx: McsParser.NumberContext): McsResult<Unit> {
        val text = ctx.DOUBLE()?.text
        (text?.toDoubleOrNull() ?: return McsResult.ofErrors("Cannot parse $text as a number."))
        tail.append(text)
        return McsResult.ofValue()
    }

    public override fun visitAddition(ctx: McsParser.AdditionContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { tail.append('+') }

    public override fun visitMultiplication(ctx: McsParser.MultiplicationContext): McsResult<Unit> {
        val left = ctx.expression(0)!!
        val right = ctx.expression(1)!!

        return visitBinaryInstruction(left, right) {
            if (right is McsParser.NumberContext) {
                tail.append("\\times ")
                return@visitBinaryInstruction
            }

            tail.append("\\:")
        }
    }

    public override fun visitDivision(ctx: McsParser.DivisionContext): McsResult<Unit> {
        val errors = mutableListOf<String>()
        tail.append("\\frac{")
        var left = ctx.expression(0)!!

        if (left is McsParser.SubexpressionContext)
            left = left.expression()!!

        visitExpression(left).ifErrors { errors.addAll(it) }
        tail.append("}{")
        var right = ctx.expression(1)!!

        if (right is McsParser.SubexpressionContext)
            right = right.expression()!!

        visitExpression(right).ifErrors { errors.addAll(it) }

        if (errors.isNotEmpty())
            return McsResult.ofErrors(errors)

        tail.append('}')
        return McsResult.ofValue()
    }

    public override fun visitSubtraction(ctx: McsParser.SubtractionContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { tail.append('-') }

    private inline fun visitBinaryInstruction(
        left: McsParser.ExpressionContext,
        right: McsParser.ExpressionContext,
        generator: () -> Unit
    ): McsResult<Unit> {
        val errors = mutableListOf<String>()
        visitExpression(left).ifErrors { errors.addAll(it) }
        generator()

        val needParentheses = when {
            right is McsParser.NumberContext && !evaluateSign(right.sign()!!) -> true
            right is McsParser.EContext && !evaluateSign(right.sign()!!) -> true
            right is McsParser.PiContext && !evaluateSign(right.sign()!!) -> true
            right is McsParser.SubexpressionContext && !evaluateSign(right.sign()!!) -> true
            right is McsParser.ArgumentContext && !evaluateSign(right.sign()!!) -> true
            right is McsParser.FunctionCallContext && !evaluateSign(right.sign()!!) -> true
            else -> false
        }

        if (needParentheses)
            tail.append('(')

        visitExpression(right).ifErrors { errors.addAll(it) }

        if (needParentheses)
            tail.append(')')

        if (errors.isNotEmpty())
            return McsResult.ofErrors(errors)

        return McsResult.ofValue()
    }

    private companion object {
        private data class McsSignature(val name: String, val arity: Int)

        private val functionAdapters: Map<McsSignature, McsTexGenerator.(List<McsParser.ExpressionContext>) -> McsResult<Unit>> =
            mapOf(
                ms("abs", 1) to f@{ args ->
                    tail.append('|')
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append('|')
                    McsResult.ofValue()
                },

                ms("acos", 1) to inverseSimple("cos"),
                ms("acot", 1) to inverseSimple("cot"),
                ms("acsc", 1) to inverseSimple("csc"),
                ms("asec", 1) to inverseSimple("sec"),
                ms("asin", 1) to inverseSimple("sin"),
                ms("atan", 1) to inverseSimple("tan"),

                ms("cbrt", 1) to f@{ args ->
                    tail.append("\\sqrt[3]{")
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append("}")
                    McsResult.ofValue()
                },

                ms("cos", 1) to simple("cos"),
                ms("cot", 1) to simple("cot"),
                ms("csc", 1) to simple("csc"),

                ms("exp", 1) to f@{ args ->
                    tail.append("e^{")
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append('}')
                    McsResult.ofValue()
                },

                ms("h", 1) to simple("H"),

                ms("log", 2) to f@{ args ->
                    tail.append("log_{")
                    val errors = mutableListOf<String>()
                    visitExpression(args.first()).ifErrors { errors.addAll(it) }
                    tail.append("}(")
                    visitExpression(args[1]).ifErrors { errors.addAll(it) }

                    if (errors.isNotEmpty())
                        return@f McsResult.ofErrors(errors)

                    tail.append(')')
                    McsResult.ofValue()
                },

                ms("log10", 1) to f@{ args ->
                    tail.append("log_{10}(")
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append(')')
                    McsResult.ofValue()
                },

                ms("log2", 1) to f@{ args ->
                    tail.append("log_{2}(")
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append(')')
                    McsResult.ofValue()
                },

                ms("ln", 1) to simple("log"),

                ms("root", 2) to f@{ args ->
                    tail.append("\\sqrt[")
                    val errors = mutableListOf<String>()
                    visitExpression(args.first()).ifErrors { errors.addAll(it) }
                    tail.append("]{")
                    visitExpression(args[1]).ifErrors { errors.addAll(it) }

                    if (errors.isNotEmpty())
                        return@f McsResult.ofErrors(errors)

                    tail.append("}")
                    McsResult.ofValue()
                },

                ms("sec", 1) to simple("sec"),
                ms("sin", 1) to simple("sin"),

                ms("sqrt", 1) to f@{ args ->
                    tail.append("\\sqrt{")
                    visitExpression(args.first()).ifErrors { return@f McsResult.ofErrors(it) }
                    tail.append("}")
                    McsResult.ofValue()
                },

                ms("tan", 1) to simple("tan")
            )

        private fun adapterOf(
            name: String,
            arity: Int
        ): (McsTexGenerator.(List<McsParser.ExpressionContext>) -> McsResult<Unit>)? =
            functionAdapters[McsSignature(name, arity)]

        private fun inverseSimple(name: String): McsTexGenerator.(List<McsParser.ExpressionContext>) -> McsResult<Unit> =
            f@{ args ->
                tail.append(name)
                tail.append("^{-1}(")
                val errors = mutableListOf<String>()

                args.forEachIndexed { idx, arg ->
                    visitExpression(arg).ifErrors { errors.addAll(it) }

                    if (idx != args.lastIndex)
                        tail.append(',')
                }

                if (errors.isNotEmpty())
                    return@f McsResult.ofErrors(errors)

                tail.append(')')
                McsResult.ofValue()
            }

        private fun simple(name: String): McsTexGenerator.(List<McsParser.ExpressionContext>) -> McsResult<Unit> =
            f@{ args ->
                if (parentForFunctionIsExponentiation) {
                    parentForFunctionIsExponentiation = false
                    val exponent = StringBuilder(tail)
                    tail.clear()
                    tail.append(name)
                    tail.append("^{")
                    tail.append(exponent)
                    tail.append("}(")
                    val errors = mutableListOf<String>()

                    args.forEachIndexed { idx, arg ->
                        visitExpression(arg).ifErrors { errors.addAll(it) }

                        if (idx != args.lastIndex)
                            tail.append(',')
                    }

                    tail.append(')')
                    childFunctionTookExponentiation = true
                    return@f McsResult.ofValue()
                }

                tail.append(name)
                tail.append('(')
                val errors = mutableListOf<String>()

                args.forEachIndexed { idx, arg ->
                    visitExpression(arg).ifErrors { errors.addAll(it) }

                    if (idx != args.lastIndex)
                        tail.append(',')
                }

                tail.append(')')
                McsResult.ofValue()
            }

        private fun ms(name: String, arity: Int): McsSignature = McsSignature(name, arity)
    }
}
