package io.github.commandertvis.mcs

import net.jafama.FastMath

internal object Math {
    private const val LN2 = 0.6931471805599453

    @JvmStatic
    public fun acot(x: Double): Double = FastMath.atan(1.0 / x)

    @JvmStatic
    public fun acsc(x: Double): Double = FastMath.asin(1.0 / x)

    @JvmStatic
    public fun asec(x: Double): Double = FastMath.acos(1.0 / x)

    @JvmStatic
    public fun cot(x: Double): Double = FastMath.cos(x) / FastMath.sin(x)

    @JvmStatic
    public fun csc(x: Double): Double = 1 / FastMath.sin(x)

    @JvmStatic
    public fun h(x: Double): Double {
        if (x >= 0.0)
            return 1.0

        return 0.0
    }

    @JvmStatic
    public fun log(x: Double, base: Double): Double {
        if (base <= 0.0 || base == 1.0)
            return Double.NaN

        return FastMath.log(x) / FastMath.log(base)
    }


    @JvmStatic
    public fun log2(x: Double): Double = FastMath.log(x) / LN2

    @JvmStatic
    public fun root(exponent: Double, base: Double): Double = FastMath.pow(base, 1.0 / exponent)

    @JvmStatic
    public fun sec(x: Double): Double = 1.0 / FastMath.cos(x)
}
