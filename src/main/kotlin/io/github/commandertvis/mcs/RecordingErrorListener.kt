package io.github.commandertvis.mcs

import org.antlr.v4.runtime.BaseErrorListener
import org.antlr.v4.runtime.RecognitionException
import org.antlr.v4.runtime.Recognizer

internal class RecordingErrorListener : BaseErrorListener() {
    internal val tail: MutableList<String> = mutableListOf()

    public override fun syntaxError(
        recognizer: Recognizer<*, *>,
        offendingSymbol: Any?,
        line: Int,
        charPositionInLine: Int,
        msg: String,
        e: RecognitionException?
    ): Unit = run { tail += "line $line:$charPositionInLine $msg" }
}
