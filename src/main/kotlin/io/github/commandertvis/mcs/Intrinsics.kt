package io.github.commandertvis.mcs

internal object Intrinsics {
    private const val IO_GITHUB_COMMANDERTVIS_MCS_MATH = "io/github/commandertvis/mcs/Math"
    private const val NET_JAFAMA_FASTMATH = "net/jafama/FastMath"

    internal data class McsSignature(val name: String, val arity: Int)

    internal data class JavaSignature(val owner: String, val name: String, val descriptor: String)

    private val intrinsics: Map<McsSignature, JavaSignature> = hashMapOf(
        ms("abs", 1) to js(NET_JAFAMA_FASTMATH, "abs", "(D)D"),
        ms("acos", 1) to js(NET_JAFAMA_FASTMATH, "acos", "(D)D"),
        ms("acot", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "acot", "(D)D"),
        ms("acsc", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "acsc", "(D)D"),
        ms("asec", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "asec", "(D)D"),
        ms("asin", 1) to js(NET_JAFAMA_FASTMATH, "asin", "(D)D"),
        ms("atan", 1) to js(NET_JAFAMA_FASTMATH, "atan", "(D)D"),
        ms("cbrt", 1) to js(NET_JAFAMA_FASTMATH, "cbrt", "(D)D"),
        ms("cos", 1) to js(NET_JAFAMA_FASTMATH, "cos", "(D)D"),
        ms("cot", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "cot", "(D)D"),
        ms("csc", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "csc", "(D)D"),
        ms("exp", 1) to js(NET_JAFAMA_FASTMATH, "exp", "(D)D"),
        ms("h", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "h", "(D)D"),
        ms("log", 2) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "log", "(DD)D"),
        ms("log10", 1) to js(NET_JAFAMA_FASTMATH, "log10", "(D)D"),
        ms("log2", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "log2", "(D)D"),
        ms("ln", 1) to js(NET_JAFAMA_FASTMATH, "log", "(D)D"),
        ms("root", 2) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "root", "(DD)D"),
        ms("sec", 1) to js(IO_GITHUB_COMMANDERTVIS_MCS_MATH, "sec", "(D)D"),
        ms("sin", 1) to js(NET_JAFAMA_FASTMATH, "sin", "(D)D"),
        ms("sqrt", 1) to js(NET_JAFAMA_FASTMATH, "sqrt", "(D)D"),
        ms("tan", 1) to js(NET_JAFAMA_FASTMATH, "tan", "(D)D")
    )

    internal fun signatureOf(name: String, arity: Int): JavaSignature? = intrinsics[McsSignature(name, arity)]

    private fun ms(name: String, arity: Int): McsSignature = McsSignature(name, arity)

    private fun js(owner: String, name: String, descriptor: String): JavaSignature =
        JavaSignature(owner, name, descriptor)
}
