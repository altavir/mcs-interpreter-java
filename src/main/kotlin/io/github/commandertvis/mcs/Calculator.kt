package io.github.commandertvis.mcs

internal interface Calculator {
    /**
     * Evaluates a certain operation for [x].
     *
     * @param x the argument.
     * @return the value.
     */
    public fun evaluate(x: Double): Double
}
