package io.github.commandertvis.mcs

import McsParserBaseVisitor
import net.jafama.FastMath
import org.objectweb.asm.ClassWriter
import org.objectweb.asm.Label
import org.objectweb.asm.MethodVisitor
import org.objectweb.asm.Opcodes.*
import java.io.File

internal class McsClassGenerator : McsParserBaseVisitor<McsResult<Unit>>() {
    private inner class ClassLoader(parent: java.lang.ClassLoader) : java.lang.ClassLoader(parent) {
        fun defineClass(name: String?, b: ByteArray): Class<*> = defineClass(name, b, 0, b.size)
    }

    private val classLoader = ClassLoader(javaClass.classLoader)
    private lateinit var calculatorWriter: ClassWriter
    private lateinit var evaluateVisitor: MethodVisitor
    private var ms: Int = 1

    @Suppress("UNCHECKED_CAST")
    @Synchronized
    internal fun instantiateCalculator(formula: String, ctx: McsParser.ProgramContext): McsResult<Calculator> {
        try {
            val c = Class.forName("io.github.commandertvis.mcs.C${formula.hashCode()}") as Class<out Calculator>
            return McsResult.ofValue(c.newInstance())
        } catch (ignored: ClassNotFoundException) {
        }

        calculatorWriter = ClassWriter(0)

        calculatorWriter.visit(
            V1_8,
            ACC_PUBLIC + ACC_FINAL,
            "io/github/commandertvis/mcs/C${formula.hashCode()}",
            null,
            "java/lang/Object",
            arrayOf("io/github/commandertvis/mcs/Calculator")
        )

        calculatorWriter.visitMethod(
            ACC_PUBLIC,
            "<init>",
            "()V",
            null,
            null
        ).run {
            visitCode()
            val l0 = Label()
            visitLabel(l0)
            visitVarInsn(ALOAD, 0)

            visitMethodInsn(
                INVOKESPECIAL,
                "java/lang/Object",
                "<init>",
                "()V",
                false
            )

            visitInsn(RETURN)
            val l1 = Label()
            visitLabel(l1)
            visitLocalVariable("this", "Lio/github/commandertvis/mcs/C${formula.hashCode()};", null, l0, l1, 0)
            visitMaxs(1, 1)
            visitEnd()
        }

        evaluateVisitor = calculatorWriter.visitMethod(ACC_PUBLIC + ACC_FINAL, "evaluate", "(D)D", null, null)

        evaluateVisitor.run {
            visitCode()
            val l0 = Label()
            visitLabel(l0)
            visitProgram(ctx).ifErrors { return McsResult.ofErrors(it) }
            visitInsn(DRETURN)
            val l1 = Label()
            visitLabel(l1)
            visitLocalVariable("this", "Lio/github/commandertvis/mcs/C${formula.hashCode()};", null, l0, l1, 0)
            visitLocalVariable("x", "D", null, l0, l1, 1)
            visitMaxs(ms, 3)
            visitEnd()
        }

        calculatorWriter.visitEnd()
        ms = 1

        return McsResult.ofValue(
            (classLoader
                .defineClass("io.github.commandertvis.mcs.C${formula.hashCode()}", calculatorWriter.toByteArray())
                .newInstance() as Calculator)
        )
    }

    public override fun visitProgram(ctx: McsParser.ProgramContext): McsResult<Unit> {
        return visitExpression(ctx.expression()!!)
    }

    private fun visitExpression(ctx: McsParser.ExpressionContext): McsResult<Unit> = when (ctx) {
        is McsParser.ExponentiationContext -> visitExponentiation(ctx)
        is McsParser.MultiplicationContext -> visitMultiplication(ctx)
        is McsParser.DivisionContext -> visitDivision(ctx)
        is McsParser.AdditionContext -> visitAddition(ctx)
        is McsParser.SubtractionContext -> visitSubtraction(ctx)
        is McsParser.NumberContext -> visitNumber(ctx)
        is McsParser.EContext -> visitE(ctx)
        is McsParser.PiContext -> visitPi(ctx)
        is McsParser.ArgumentContext -> visitArgument(ctx)
        is McsParser.SubexpressionContext -> visitSubexpression(ctx)
        is McsParser.FunctionCallContext -> visitFunctionCall(ctx)
        else -> McsResult.ofErrors("Illegal expression context: $ctx")
    }

    public override fun visitFunctionCall(ctx: McsParser.FunctionCallContext): McsResult<Unit> =
        visitSignedExpression(ctx.sign()!!) f@{
            val name = ctx.IDENTIFIER()!!.text
            val arity = ctx.expression().size

            val sig = Intrinsics.signatureOf(name, arity)
                ?: return McsResult.ofErrors("Cannot find function $name with arity $arity")

            val errors = mutableListOf<String>()
            ctx.expression().forEach { arg -> visitExpression(arg).ifErrors { errors.addAll(it) } }

            if (errors.isNotEmpty())
                return@f McsResult.ofErrors(errors)

            evaluateVisitor.visitMethodInsn(INVOKESTATIC, sig.owner, sig.name, sig.descriptor, false)
            ms++
            McsResult.ofValue()
        }

    public override fun visitSubexpression(ctx: McsParser.SubexpressionContext): McsResult<Unit> =
        visitSignedExpression(ctx.sign()!!) { visitExpression(ctx) }

    public override fun visitArgument(ctx: McsParser.ArgumentContext): McsResult<Unit> {
        ms++

        return visitSignedExpression(ctx.sign()!!) {
            evaluateVisitor.visitVarInsn(DLOAD, 1)
            McsResult.ofValue()
        }
    }

    private inline fun visitSignedExpression(
        sign: McsParser.SignContext,
        generator: () -> McsResult<Unit>
    ): McsResult<Unit> {
        val signValue = evaluateSign(sign)

        if (signValue == (-1).toByte())
            evaluateVisitor.visitLdcInsn(-1.0)

        generator().ifErrors { return McsResult.ofErrors(it) }

        if (signValue == (-1).toByte()) {
            evaluateVisitor.visitInsn(DMUL)
            ms += 2
        }

        return McsResult.ofValue()
    }

    public override fun visitExponentiation(ctx: McsParser.ExponentiationContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) {
            evaluateVisitor.visitMethodInsn(INVOKESTATIC, "net/jafama/FastMath", "pow", "(DD)D", false)
        }

    private fun evaluateSign(ctx: McsParser.SignContext): Byte {
        val sign = ctx.text

        if (sign.count { it == '-' } % 2 == 0)
            return 1

        return -1
    }

    public override fun visitPi(ctx: McsParser.PiContext): McsResult<Unit> = visitConstant(
        FastMath.PI,
        ctx.sign()!!
    )

    public override fun visitE(ctx: McsParser.EContext): McsResult<Unit> = visitConstant(
        FastMath.E,
        ctx.sign()!!
    )

    public override fun visitNumber(ctx: McsParser.NumberContext): McsResult<Unit> {
        val text = ctx.DOUBLE()?.text

        return visitConstant(
            text?.toDoubleOrNull() ?: return McsResult.ofErrors("Cannot parse $text as a number."),
            ctx.sign()!!
        )
    }

    private fun visitConstant(value: Double, sign: McsParser.SignContext): McsResult<Unit> {
        var constant = value

        if (evaluateSign(sign) == (-1).toByte())
            constant *= -1.0

        evaluateVisitor.visitLdcInsn(value)
        ms++
        return McsResult.ofValue()
    }

    public override fun visitAddition(ctx: McsParser.AdditionContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { evaluateVisitor.visitInsn(DADD) }

    public override fun visitMultiplication(ctx: McsParser.MultiplicationContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { evaluateVisitor.visitInsn(DMUL) }

    public override fun visitDivision(ctx: McsParser.DivisionContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { evaluateVisitor.visitInsn(DDIV) }

    public override fun visitSubtraction(ctx: McsParser.SubtractionContext): McsResult<Unit> =
        visitBinaryInstruction(ctx.expression(0)!!, ctx.expression(1)!!) { evaluateVisitor.visitInsn(DSUB) }

    private inline fun visitBinaryInstruction(
        p1: McsParser.ExpressionContext,
        p2: McsParser.ExpressionContext,
        generator: () -> Unit
    ): McsResult<Unit> {
        val errors = mutableListOf<String>()
        visitExpression(p1).ifErrors { errors.addAll(it) }
        visitExpression(p2).ifErrors { errors.addAll(it) }

        if (errors.isNotEmpty())
            return McsResult.ofErrors(errors)

        generator()
        ms++
        return McsResult.ofValue()
    }
}
