package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import kotlin.test.assertTrue

internal class TestErrorListeners {
    @Test
    public fun `test lexer error`() {
        val i = McsInterpreter(parallelism = 1)
        val result = i.evaluate("бум", 1.0)
        assertTrue(result.hasErrors())
    }

    @Test
    public fun `test parser error`() {
        val i = McsInterpreter(parallelism = 1)
        val result = i.evaluate("sin(x", 1.0)
        assertTrue(result.hasErrors())
    }

    @Test
    public fun `test not existing function error`() {
        val i = McsInterpreter(parallelism = 1)
        val result = i.evaluate("notexists(x)", 1.0)
        assertTrue(result.hasErrors())
    }
}
