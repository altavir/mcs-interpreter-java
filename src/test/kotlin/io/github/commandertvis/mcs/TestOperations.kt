package io.github.commandertvis.mcs

import net.jafama.FastMath
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TestOperations {
    @Test
    public fun `test function call`(): Unit = assertEquals(
        expected = FastMath.sin(FastMath.PI),
        actual = parseAndCompile("sin(x)").evaluate(FastMath.PI)
    )

    @Test
    public fun `test addition`(): Unit = assertEquals(expected = 4.0, actual = parseAndCompile("x+x").evaluate(x = 2.0))

    @Test
    public fun `test subtraction`(): Unit =
        assertEquals(expected = 0.0, actual = parseAndCompile("x-x").evaluate(x = 2.0))

    @Test
    public fun `test multiplication`(): Unit = assertEquals(
        expected = 4.0,
        actual = parseAndCompile("x*x").evaluate(2.0)
    )

    @Test
    public fun `test division`(): Unit = assertEquals(expected = 1.0, actual = parseAndCompile("x/x").evaluate(x = 2.0))
}

