package io.github.commandertvis.mcs

import net.jafama.FastMath
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TestCustomFunctions {
    @Test
    public fun `test acot`(): Unit = assertEquals(
        expected = 0.0,
        actual = parseAndCompile("acot(x)").evaluate(Double.NEGATIVE_INFINITY)
    )

    @Test
    public fun `test asec`(): Unit = assertEquals(
        expected = FastMath.PI,
        actual = parseAndCompile("asec(x)").evaluate(-1.0)
    )

    @Test
    public fun `test cot`(): Unit = assertEquals(
        expected = Math.cot(0.0),
        actual = parseAndCompile("cot(x)").evaluate(0.0)
    )

    @Test
    public fun `test h`() {
        val calc = parseAndCompile("h(x)")
        assertEquals(expected = 0.0, actual = calc.evaluate(-1.0))
        assertEquals(expected = 1.0, actual = calc.evaluate(0.0))
    }

    @Test
    public fun `test root`(): Unit = assertEquals(expected = 3.0, actual = parseAndCompile("root(3,x)").evaluate(27.0))

    @Test
    public fun `test sec`(): Unit = assertEquals(expected = -1.0, actual = parseAndCompile("sec(pi)").evaluate(0.0))
}
