package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class TestCaching {
    @Test
    public fun `test the class is not loaded twice`(): Unit =
        assertEquals(parseAndCompile("x")::class.qualifiedName, parseAndCompile("x")::class.qualifiedName)

    @Test
    public fun `test the calculator instances are cached in interpreter, and the cache is not overloaded`() {
        val a = McsInterpreter(parallelism = 1, cacheSize = 3)
        a.evaluate("x", 1.0)
        a.evaluate("x^2", 1.0)
        a.evaluate("x^3", 1.0)
        assertTrue(a.compilerCache.size == 3)
        a.evaluate("x^4", 1.0)
        assertTrue(a.compilerCache.size == 3)
    }
}
