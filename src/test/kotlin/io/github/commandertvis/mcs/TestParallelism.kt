package io.github.commandertvis.mcs

import net.jafama.FastMath
import org.junit.jupiter.api.Test
import kotlin.test.assertTrue
import kotlin.test.fail

internal class TestParallelism {
    @Test
    public fun `test linear histogram 2 thr`() {
        val hist = McsInterpreter(2).histogram(
            expression = "x",
            nValues = 1000000,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )

        hist.value?.forEach { assertTrue(actual = FastMath.abs(0.1 - it) < 0.05, message = "Too big error") }
            ?: fail((hist.errors ?: fail()).joinToString())
    }


    @Test
    public fun `test linear histogram 3 thr`() {
        val hist = McsInterpreter(3).histogram(
            expression = "x",
            nValues = 1000000,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )

        hist.value?.forEach { assertTrue(actual = FastMath.abs(0.1 - it) < 0.05, message = "Too big error") }
            ?: fail((hist.errors?: fail()).joinToString())
    }
}