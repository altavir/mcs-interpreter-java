package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.fail

internal class TestMcsTexGenerator {
    private val interpreter = McsInterpreter()

    @Test
    public fun `test 3*sin(2*x)*cos(x)^4`() {
        assertEquals(
            expected = "3\\:sin(2\\:x)\\:cos^{4}(x)",
            actual = interpreter.produceTex("3*sin(2*x)*cos(x)^4").value ?: fail()
        )
    }

    @Test
    public fun `test (5*ln(5*x)^4) divided by x`() {
        assertEquals(
            expected = "\\frac{5\\:log^{4}(5\\:x)}{x}",
            actual = interpreter.produceTex("(5*ln(5*x)^4)/x").value ?: fail()
        )
    }

    @Test
    public fun `test acos(root(6,1-x))`() {
        assertEquals(
            expected = "cos^{-1}(\\sqrt[6]{1-x})",
            actual = interpreter.produceTex("acos(root(6,1-x))").value ?: fail()
        )
    }

    @Test
    public fun `test e*root(5,x) divided by 5`() {
        assertEquals(
            expected = "\\frac{e\\:\\sqrt[5]{x}}{5}",
            actual = interpreter.produceTex("e*root(5,x)/5").value ?: fail()
        )
    }
}
