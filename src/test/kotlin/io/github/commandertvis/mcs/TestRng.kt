package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class TestRng {
    @Test
    public fun `test RNG`() {
        val rng = RNG()
        assertEquals(expected = 0.6938893903907233, actual = rng.nextDouble(0.0, 1.0))
        assertEquals(expected = 0.9377119177015636, actual = rng.nextDouble(0.0, 1.0))
    }
}
