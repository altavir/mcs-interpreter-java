package io.github.commandertvis.mcs

import McsLexer
import McsParser
import org.antlr.v4.runtime.CharStreams
import org.antlr.v4.runtime.CommonTokenStream
import kotlin.test.fail

internal fun parseAndCompile(expression: String): Calculator {
    val listener = RecordingErrorListener()

    val parser = McsParser(
        CommonTokenStream(
            McsLexer(CharStreams.fromString(expression)).apply { removeErrorListeners(); addErrorListener(listener) })
    ).apply { removeErrorListeners(); addErrorListener(listener) }

    val program = parser.program()

    if (listener.tail.isNotEmpty())
        fail(listener.tail.toString())

    return McsClassGenerator().instantiateCalculator(expression, program).value ?: fail()
}