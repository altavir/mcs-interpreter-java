package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow

internal class TestPossibleOperandStackOverflow {
    @Test
    fun `test possible operand stack overflow`() {
        assertDoesNotThrow {
             McsInterpreter().evaluate("2*x^2+3*x+4", 1.0)
        }
    }
}
