package io.github.commandertvis.mcs

import net.jafama.FastMath
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.test.fail

internal class TestMcsInterpreter {
    @Test
    public fun `test evaluate`() {
        assertEquals(
            expected = 1.0,
            actual = McsInterpreter().evaluate(expression = "x", x = 1.0).value ?: fail()
        )
    }

    @Test
    public fun `test linear histogram`() {
        val hist = McsInterpreter().histogram(
            expression = "x",
            nValues = 1000000,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )

        hist.value?.forEach { assertTrue(actual = FastMath.abs(0.1 - it) < 0.05, message = "Too big error") }
            ?: fail((hist.errors ?: fail()).joinToString())
    }

    @Test
    public fun `test linear plot`() {
        val plot = McsInterpreter().plot(expression = "x", fromInclusive = 0.0, toExclusive = 1.0, nPoints = 3).value
            ?: fail()

        assertEquals(expected = 0.0, actual = plot[0.0])
        assertEquals(expected = 1.0 / 3.0, actual = plot[1.0 / 3.0])
        assertEquals(expected = 2.0 / 3.0, actual = plot[2.0 / 3.0])
    }
}
