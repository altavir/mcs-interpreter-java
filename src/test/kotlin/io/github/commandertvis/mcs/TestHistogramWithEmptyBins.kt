package io.github.commandertvis.mcs

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.fail

internal class TestHistogramWithEmptyBins {
    @Test
    public fun `test histogram with empty bins`() {
        val hist = McsInterpreter(2).histogram(
            expression = "-x",
            nValues = 1000000,
            xMin = 0.0,
            xMax = 1.0,
            histogramMin = 0.0,
            histogramMax = 1.0,
            nBins = 10
        )

        hist.value?.forEach { assertEquals(0.0, it) } ?: fail((hist.errors?:fail()).joinToString())
    }
}